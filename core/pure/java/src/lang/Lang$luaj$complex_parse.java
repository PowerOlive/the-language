package lang;
public class Lang$luaj$complex_parse extends org.luaj.vm2.lib.VarArgFunction {
    org.luaj.vm2.LuaValue u0;
    org.luaj.vm2.LuaValue u1;
    org.luaj.vm2.LuaValue[] u2;
    org.luaj.vm2.LuaValue u3;
    org.luaj.vm2.LuaValue u4;
    org.luaj.vm2.LuaValue u5;
    org.luaj.vm2.LuaValue u6;
    org.luaj.vm2.LuaValue u7;
    org.luaj.vm2.LuaValue[] u8;
    org.luaj.vm2.LuaValue u9;
    org.luaj.vm2.LuaValue u10;
    org.luaj.vm2.LuaValue u11;
    org.luaj.vm2.LuaValue u12;
    org.luaj.vm2.LuaValue u13;
    org.luaj.vm2.LuaValue[] u14;
    org.luaj.vm2.LuaValue u15;
    org.luaj.vm2.LuaValue u16;
    org.luaj.vm2.LuaValue u17;
    org.luaj.vm2.LuaValue u18;
    org.luaj.vm2.LuaValue u19;
    org.luaj.vm2.LuaValue[] u20;
    org.luaj.vm2.LuaValue[] u21;
    org.luaj.vm2.LuaValue u22;
    org.luaj.vm2.LuaValue u23;
    org.luaj.vm2.LuaValue u24;
    org.luaj.vm2.LuaValue u25;
    org.luaj.vm2.LuaValue u26;
    org.luaj.vm2.LuaValue u27;
    org.luaj.vm2.LuaValue u28;
    org.luaj.vm2.LuaValue u29;
    org.luaj.vm2.LuaValue u30;
    org.luaj.vm2.LuaValue u31;
    final static org.luaj.vm2.LuaValue k0;
    final static org.luaj.vm2.LuaValue k1;
    final static org.luaj.vm2.LuaValue k2;
    final static org.luaj.vm2.LuaValue k3;
    final static org.luaj.vm2.LuaValue k4;
    
    static {
        k0 = org.luaj.vm2.LuaValue.valueOf(0);
        k1 = org.luaj.vm2.LuaString.valueOf("$");
        k2 = org.luaj.vm2.LuaString.valueOf("%");
        k3 = org.luaj.vm2.LuaString.valueOf("@");
        k4 = org.luaj.vm2.LuaString.valueOf("^");
    }
    
    public Lang$luaj$complex_parse() {
    }
    
    final public org.luaj.vm2.Varargs onInvoke(org.luaj.vm2.Varargs a) {
        org.luaj.vm2.LuaValue a0 = a.arg(1);
        a.subargs(2);
        org.luaj.vm2.LuaValue a1 = org.luaj.vm2.LuaValue.NIL;
        org.luaj.vm2.LuaValue[] a2 = Lang$luaj$complex_parse.newupe();
        a2[0] = a1;
        org.luaj.vm2.LuaValue[] a3 = Lang$luaj$complex_parse.newupe();
        a3[0] = a1;
        org.luaj.vm2.LuaValue[] a4 = Lang$luaj$complex_parse.newupe();
        a4[0] = a1;
        org.luaj.vm2.LuaValue[] a5 = Lang$luaj$complex_parse.newupe();
        a5[0] = a1;
        org.luaj.vm2.LuaValue[] a6 = Lang$luaj$complex_parse.newupe();
        a6[0] = a1;
        org.luaj.vm2.LuaValue[] a7 = Lang$luaj$complex_parse.newupe();
        a7[0] = a1;
        org.luaj.vm2.LuaValue[] a8 = Lang$luaj$complex_parse.newupe();
        a8[0] = a1;
        org.luaj.vm2.LuaValue[] a9 = Lang$luaj$complex_parse.newupe();
        a9[0] = a1;
        org.luaj.vm2.LuaValue[] a10 = Lang$luaj$complex_parse.newupe();
        a10[0] = a1;
        org.luaj.vm2.LuaValue[] a11 = Lang$luaj$complex_parse.newupe();
        a11[0] = a1;
        Lang$luaj$complex_parse$0 a12 = new Lang$luaj$complex_parse$0();
        a12.u0 = a2;
        a12.u1 = a3;
        Lang$luaj$complex_parse$1 a13 = new Lang$luaj$complex_parse$1();
        a13.u0 = this.u0;
        a13.u1 = a12;
        a13.u2 = this.u1;
        a13.u3 = a2;
        a13.u4 = a3;
        Lang$luaj$complex_parse$2 a14 = new Lang$luaj$complex_parse$2();
        a14.u0 = this.u0;
        a14.u1 = this.u1;
        a14.u2 = a2;
        a14.u3 = a3;
        Lang$luaj$complex_parse$3 a15 = new Lang$luaj$complex_parse$3();
        a15.u0 = this.u1;
        Lang$luaj$complex_parse$4 a16 = new Lang$luaj$complex_parse$4();
        Lang$luaj$complex_parse$5 a17 = new Lang$luaj$complex_parse$5();
        a17.u0 = a12;
        a17.u1 = a13;
        a17.u2 = a16;
        a17.u3 = a14;
        Lang$luaj$complex_parse$6 a18 = new Lang$luaj$complex_parse$6();
        a18.u0 = a12;
        a18.u1 = a13;
        a18.u2 = a8;
        a18.u3 = a14;
        a18.u4 = this.u1;
        a18.u5 = this.u2;
        a18.u6 = a15;
        a18.u7 = this.u3;
        Lang$luaj$complex_parse$7 a19 = new Lang$luaj$complex_parse$7();
        a19.u0 = a12;
        a19.u1 = a13;
        a19.u2 = a14;
        a19.u3 = this.u4;
        a19.u4 = this.u5;
        a19.u5 = this.u6;
        a19.u6 = this.u7;
        a19.u7 = a17;
        a19.u8 = a15;
        a19.u9 = this.u8;
        a19.u10 = a9;
        Lang$luaj$complex_parse$8 a20 = new Lang$luaj$complex_parse$8();
        a20.u0 = a12;
        a20.u1 = a13;
        a20.u2 = a14;
        a20.u3 = a19;
        a20.u4 = a15;
        a20.u5 = this.u4;
        a20.u6 = this.u9;
        a20.u7 = this.u10;
        a20.u8 = this.u6;
        Lang$luaj$complex_parse$9 a21 = new Lang$luaj$complex_parse$9();
        a21.u0 = a12;
        a21.u1 = a13;
        a21.u2 = a14;
        a21.u3 = a19;
        a21.u4 = a15;
        a21.u5 = this.u4;
        a21.u6 = this.u11;
        a21.u7 = this.u10;
        a21.u8 = this.u6;
        Lang$luaj$complex_parse$10 a22 = new Lang$luaj$complex_parse$10();
        a22.u0 = a16;
        a8[0] = a22;
        Lang$luaj$complex_parse$11 a23 = new Lang$luaj$complex_parse$11();
        a23.u0 = a17;
        a23.u1 = a19;
        a23.u2 = a11;
        a23.u3 = a20;
        a23.u4 = a21;
        a23.u5 = a4;
        a23.u6 = a5;
        a23.u7 = a6;
        a23.u8 = a7;
        a23.u9 = a15;
        a9[0] = a23;
        Lang$luaj$complex_parse$12 a24 = new Lang$luaj$complex_parse$12();
        a24.u0 = a15;
        Lang$luaj$complex_parse$13 a25 = new Lang$luaj$complex_parse$13();
        a25.u0 = a12;
        Lang$luaj$complex_parse$14 a26 = new Lang$luaj$complex_parse$14();
        a26.u0 = a24;
        a26.u1 = a25;
        a26.u2 = a13;
        Lang$luaj$complex_parse$15 a27 = new Lang$luaj$complex_parse$15();
        a27.u0 = a26;
        a27.u1 = a19;
        a27.u2 = a18;
        a27.u3 = a20;
        a27.u4 = a21;
        a27.u5 = a4;
        a27.u6 = a5;
        a27.u7 = a6;
        a27.u8 = a7;
        a27.u9 = a10;
        a27.u10 = a15;
        a27.u11 = a12;
        a27.u12 = a13;
        a27.u13 = this.u12;
        a27.u14 = this.u13;
        a27.u15 = this.u14;
        a27.u16 = this.u15;
        a27.u17 = this.u16;
        a27.u18 = this.u7;
        a27.u19 = this.u17;
        a27.u20 = a14;
        a27.u21 = this.u18;
        a27.u22 = this.u19;
        a27.u23 = this.u5;
        a27.u24 = a24;
        a27.u25 = a25;
        a27.u26 = this.u20;
        a27.u27 = this.u21;
        a27.u28 = this.u22;
        a10[0] = a27;
        Lang$luaj$complex_parse$16 a28 = new Lang$luaj$complex_parse$16();
        a28.u0 = a10;
        a28.u1 = this.u23;
        a28.u2 = this.u24;
        a11[0] = a28;
        a2[0] = a0;
        a3[0] = k0;
        Lang$luaj$complex_parse$make_read_two a29 = new Lang$luaj$complex_parse$make_read_two();
        a29.u0 = a12;
        a29.u1 = a13;
        a29.u2 = a14;
        a29.u3 = a19;
        a29.u4 = a15;
        a29.u5 = this.u4;
        a29.u6 = this.u6;
        a29.u7 = this.u25;
        a29.u8 = this.u10;
        Lang$luaj$complex_parse$make_read_three a30 = new Lang$luaj$complex_parse$make_read_three();
        a30.u0 = a12;
        a30.u1 = a13;
        a30.u2 = a14;
        a30.u3 = a19;
        a30.u4 = a15;
        a30.u5 = this.u4;
        a30.u6 = this.u6;
        a30.u7 = this.u25;
        a30.u8 = this.u10;
        org.luaj.vm2.LuaValue a31 = k1;
        Lang$luaj$complex_parse$19 a32 = new Lang$luaj$complex_parse$19();
        a32.u0 = this.u26;
        a32.u1 = a15;
        a32.u2 = this.u27;
        a4[0] = ((org.luaj.vm2.LuaValue)a29).call(a31, (org.luaj.vm2.LuaValue)a32);
        org.luaj.vm2.LuaValue a33 = k2;
        Lang$luaj$complex_parse$20 a34 = new Lang$luaj$complex_parse$20();
        a34.u0 = this.u28;
        a34.u1 = a15;
        a34.u2 = this.u29;
        a5[0] = ((org.luaj.vm2.LuaValue)a29).call(a33, (org.luaj.vm2.LuaValue)a34);
        org.luaj.vm2.LuaValue a35 = k3;
        Lang$luaj$complex_parse$21 a36 = new Lang$luaj$complex_parse$21();
        a36.u0 = this.u28;
        a36.u1 = a15;
        a36.u2 = this.u26;
        a36.u3 = this.u30;
        a6[0] = ((org.luaj.vm2.LuaValue)a30).call(a35, (org.luaj.vm2.LuaValue)a36);
        org.luaj.vm2.LuaValue a37 = k4;
        Lang$luaj$complex_parse$22 a38 = new Lang$luaj$complex_parse$22();
        a38.u0 = this.u28;
        a38.u1 = a15;
        a38.u2 = this.u31;
        a7[0] = ((org.luaj.vm2.LuaValue)a29).call(a37, (org.luaj.vm2.LuaValue)a38);
        return org.luaj.vm2.LuaValue.tailcallOf(a9[0], (org.luaj.vm2.Varargs)org.luaj.vm2.LuaValue.NONE);
    }
}
